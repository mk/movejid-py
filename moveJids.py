#!/usr/bin/env python2

import warnings
warnings.filterwarnings("ignore")

from xmpp import client, roster
import sys
import getpass

def exitMsg(msg):
    print msg+", exiting..."
    sys.exit(1)

def migrateConnect( jid ):
    xmppUser, xmppServer = jid.split('@')
    global xmppClient
    xmppClient = client.Client(xmppServer, debug=[])
    if xmppClient.connect() is not "tls": exitMsg("tls error")
    if not xmppClient.auth(xmppUser, getpass.getpass()): exitMsg("auth error")
    print "connected to "+xmppServer


sys.stdout.write("Export Roster from which JID? ")
inData = sys.stdin.readline()[:-1]
migrateConnect( inData )

migrateRoster = dict()   # object to migrate
xmppItems = xmppClient.getRoster().getItems()
for item in xmppItems:
    xmppGroups = xmppClient.getRoster().getGroups( item )
    if xmppGroups is None:
        xmppGroups = []
    sys.stdout.write("Migrate "+item+" ? [Y/n] ")
    if "Y" in sys.stdin.readline().upper():
        migrateRoster[item] = xmppGroups
    # TODO - send notify message

print "I am going to import the following Contacts:"
for item in migrateRoster.keys():
    print str( item ),
    print map( str, migrateRoster[item] )
xmppClient.disconnect()

sys.stdout.write( "Import Roster to which JID? " )
inData = sys.stdin.readline()[:-1]
migrateConnect( inData )

for item in migrateRoster.keys():
    if item in xmppClient.getRoster().getItems():
        # update groups
        xmppClient.getRoster().setItem(item, name=None , groups=migrateRoster[item])
    else:
        # TODO: ask for alias
        xmppClient.getRoster().setItem(item, name=None , groups=migrateRoster[item])
        # has to be authorized manually
        xmppClient.getRoster().Subscribe(item)

xmppClient.disconnect()
print "done. bye bye"
